var path = require('path')
var webpack = require('webpack')
module.exports = {
    devtool: 'cheap-module-eval-source-map',
    devServer: {
        historyApiFallback: true
    },
    entry: [
'webpack-hot-middleware/client',
'babel-polyfill',
'./client/src/js/lib/boot.jsx'
],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
          publicPath: '/'
    },
    plugins: [
new webpack.HotModuleReplacementPlugin(),
new webpack.NoEmitOnErrorsPlugin(),
new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
],
    module: {
        rules: [
          {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /(node_modules|vendor)/,
        include: /client/,
        use: 'eslint-loader'
      },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: [{loader:'babel-loader',
                options: { "presets": ["es2015", "react", "stage-3"], compact : false }
              }]
            }, {
      test: /\.s?css$/,
      use: ['style-loader', 'css-loader', 'sass-loader'],
    }, {
      test: /\.(eot|svg|ttf|woff|woff2)$/,
      use: 'file-loader',
    }, {
      test:  /\.(png|jpe?g|gif|svg)$/,
      use: [

        {
          loader: 'file-loader',
          options: {
            // path where the images will be saved
            name: 'assets/img/[name].[ext]'
          }
        },
        {
          loader: 'image-webpack-loader',
          options: {
            mozjpeg: {
            quality: 65
          },
          gifsicle: {
            optimizationLevel: 7,
            interlaced: false
          },
          optipng: {
            optimizationLevel: 7,
            interlaced: false
          }
          },
        },
      ],
    }]
	}
}
