import { createStore, applyMiddleware, compose } from 'redux'
import createLogger from 'redux-logger'
import thunk from 'redux-thunk'
import rootReducer from './reducers/index.js'
import logger from 'redux-logger'
/**
 * Entirely optional, this tiny library adds some functionality to
 * your DevTools, by logging actions/state to your console.
 */
//const logger = createLogger(); // #todo - disable on production

const createStoreWithMiddleware = compose(
  applyMiddleware(logger, thunk)
  // applyMiddleware(thunk)
)(createStore)




const configureStore = (initialState) => {

  const store = createStoreWithMiddleware(rootReducer, initialState);

  // Hot reload reducers (requires Webpack or Browserify HMR to be enabled)
  // if (module.hot) {
  //   module.hot.accept('../reducers', () =>
  //     store.replaceReducer(require('../reducers'))
  //   );
  // }

  return store;
}


export default configureStore;
