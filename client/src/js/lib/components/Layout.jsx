import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as authActions from '../store/actions/auth'
import Header from './shared/Header.jsx'


function mapStateToProps(state) {
  return {
    user: state.user,
  }
}

// Get the logout action
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authActions, dispatch),
  }
}


class Layout extends Component {

  render() {
    return (
      <div>
        <Header user={this.props.user} onLogout={this.props.actions.logout} />
          {this.props.children}
      </div>
    );
  }

}


export default connect(mapStateToProps, mapDispatchToProps)(Layout)
