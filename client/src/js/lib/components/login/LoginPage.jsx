import React, { Component, PropTypes } from 'react'
import * as authActions from '../../store/actions/auth'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'



/**
 * Turns an object whose values are 'action creators' into an object with the same
 * keys but with every action creator wrapped into a 'dispatch' call that we can invoke
 * directly later on. Here we imported the actions specified in 'CounterActions.js' and
 * used the bindActionCreators function Redux provides us.
 *
 * More info: http://redux.js.org/docs/api/bindActionCreators.html
 */

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authActions, dispatch),
  }
}


const LoginPage = React.createClass({

  getInitialState() {
    return {
      username: '',
      password: '',
      error: null,
    }
  },

  handleSubmit(e) {
    e.preventDefault();

    this.props.actions.login({
      username: this.state.username,
      password: this.state.password,
    }, (err, user) => {
      if(err) this.setState({
        error: 'Wrong credentials',
        password: '',
      })
    })
  },

  handlePasswordChange(e) {
    this.setState({password: e.target.value})
  },

  handleUsernameChange(e) {
    this.setState({username: e.target.value})
  },

  render() {
    return (
      <div className="login-container">
        <div className="columns is-hidden-tablet is-mobile pt-40">
          <div className="column is-2"></div>
          <div className="column has-text-centered">
            <h1 className="title is-3">Welcome to your Healthy Diary!</h1>
            <p>Manage you diet and activities, track the progress with your health and get pleasure :)</p>
            <hr className="mobile-hr" />
          </div>

          <div className="column is-2"></div>

        </div>

        <div className="columns">

          <div className="column is-half">
            <div className="login-box-container">
              <div className="login-box">
                <h3 className="title is-3 mt-15">Login/Sign up</h3>


                {this.state.error ?
                  <div className="notification is-warning">{this.state.error}</div>
                  : null
                }

                <form onSubmit={this.handleSubmit} role="form" method="post">
                  <label className="label">Email</label>
                  <p className="control">
                    <input name="username" className="input" type="email" autoFocus value={this.state.username} onChange={this.handleUsernameChange} />
                  </p>

                  <label className="label">Password</label>
                  <p className="control">
                    <input name="password" className="input" type="password" value={this.state.password} onChange={this.handlePasswordChange} />
                  </p>

                  <p className="control is-clearfix has-text-centered pt-40">
                    <button className="button general">
                      Go to Diary
                    </button>
                  </p>
                </form>
              </div>{/*<!-- /.login-box -->*/}

            </div>{/*<!-- /.login-box-container -->*/}
          </div>{/*<!-- /.column -->*/}

          <div className="column is-half is-hidden-mobile info-box-container">
            <div className="info-box">

              <h1 className="title is-2">Welcome to your Healthy Diary!</h1>
              <p>Manage you diet and activities, track the progress with your health and get pleasure :)</p>

              <hr />

            </div>
          </div>

        </div>{/*<!-- /.columns -->*/}

      </div>

    );
  },

})



export default connect(
  null,
  mapDispatchToProps
)(LoginPage)
