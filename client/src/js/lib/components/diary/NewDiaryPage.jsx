import React, { Component, PropTypes } from 'react'
import DatePicker from 'react-datepicker';
import { RadioGroup, Radio } from 'react-radio-group'
import moment from 'moment'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Menu from './menu/Menu.jsx'
import 'react-datepicker/dist/react-datepicker.css';

const NewDiaryPage = React.createClass({

  getInitialState() {
    return {
      startDate: moment(),
      endDate: moment().add(1, 'day'),
      showStartDateError: false,
      showEndDateError: false,
      repetition: 'day',
      showMenuForm: false,
      menuDetails: [],
      currentMenu: 1
    }
  },

  handleStartChange(e) {
    if (e >= this.state.endDate) {
      this.setState({showStartDateError : true});
      return;
    }
    this.setState({showStartDateError : false, showEndDateError : false, startDate: e});
  },

  handleEndChange(e) {
    if (e <= this.state.startDate) {
      this.setState({showEndDateError : true});
      return;
    }
    this.setState({showEndDateError : false, showStartDateError : false, endDate: e});
  },

  handleMenuRepetitionChange(e) {
      this.setState({repetition: e});
  },

  createMenuForm() {
    const daysCount = this.state.endDate.diff(this.state.startDate, 'days');
    let menusNumber = -1;
    switch (this.state.repetition) {
      case "day": menusNumber = 1; console.log('heere'); break;
      case "twoday": menusNumber = 2; break;
      case "week": menusNumber = 7; break;
      case "norep": menusNumber = daysCount; break;
    }
    console.log(daysCount);
    const dailyMenu = {
      breakfast: '',
      secondbreakfast: '',
      dinner: '',
      supper: ''
    }
    let tempMenuDetails = [];
    let loopValue = (daysCount > menusNumber) ? menusNumber: daysCount;
    console.log(loopValue);
    while (loopValue-- > 0) {
        console.log(loopValue);
        tempMenuDetails.push(dailyMenu);
    }
    this.setState({menuDetails: tempMenuDetails, showMenuForm: true});
  },

  handleMealChange(value, meal, day) {
    let tempMenuDetails = this.state.menuDetails;
    tempMenuDetails[day - 1][meal] = value;
    this.setState({menuDetails: tempMenuDetails});
  },

  render() {
    return (
      <div className="diary-container">
        <span className="fa fa-chevron-left" aria-hidden="true"></span>
        <div className="diary-box">
          <div className="has-text-centered mt-20">
            <div className="title is-4 ">Lets create new meal and activity plan!</div>
          </div>
          <div className="title is-6 mt-20">Choose dates when you will get a healthy lifestyle:</div>
          <div className="columns">
            <div className="column is-half is-hidden-mobile">
              <label className="label">From:</label>
                <DatePicker
                  inline
                  selected={this.state.startDate}
                  onChange={this.handleStartChange}
                  excludeDates={[moment(), moment().subtract(1, "days")]}
                  placeholderText="Select a date other than today or yesterday" />
            </div>
            <div className="column is-half is-hidden-mobile">
              <label className="label">To:</label>
              <DatePicker
                inline
                selected={this.state.endDate}
                onChange={this.handleEndChange}
                excludeDates={[moment(), moment().subtract(1, "days")]}
                placeholderText="Select a date other than today or yesterday" />
            </div>
          </div>
          <div className="is-mobile is-hidden-tablet has-text-centered">
            <label className="label">From:</label>
              <DatePicker
                inline
                selected={this.state.startDate}
                onChange={this.handleStartChange}
                excludeDates={[moment(), moment().subtract(1, "days")]}
                placeholderText="Select a date other than today or yesterday" />
              <label className="label">To:</label>
              <DatePicker
                inline
                selected={this.state.endDate}
                onChange={this.handleEndChange}
                excludeDates={[moment(), moment().subtract(1, "days")]}
                placeholderText="Select a date other than today or yesterday" />
          </div>
          <div className="has-text-centered">
            { this.state.showStartDateError && <small className="error-message">Selected start date should be earlier than end date and without set other plan</small> }
            { this.state.showEndDateError && <small className="error-message">Selected end date should be later than start date and without set other plan</small> }
          </div>
          <div className="title is-6 mt-20">Choose how often your daily menu should be repeated:</div>
            <RadioGroup name="repetition" selectedValue={this.state.repetition} onChange={this.handleMenuRepetitionChange}>
              <Radio value="day" />Every day
              <Radio value="twoday" />Every two days
              <Radio value="week" />Every week
              <Radio value="norep" />No repetition, different menu for each day!
            </RadioGroup>
            {
              !this.state.showMenuForm
              &&
              <div className="has-text-centered mt-40">
                <button className="button general" onClick={this.createMenuForm}>
                  Go to menu description
                </button>
              </div>
            }
            {this.state.showMenuForm &&
              <div>
                <i className="fa fa-chevron-left" aria-hidden="true"></i>
                <Menu number={this.state.currentMenu}
                      details={this.state.menuDetails[this.state.currentMenu - 1]}
                      handleChange={this.handleMealChange}/>
              <i className="fa fa-chevron-right" aria-hidden="true"></i>
            </div>
            }
        </div>
      </div>


    );
  },

})



export default NewDiaryPage;
