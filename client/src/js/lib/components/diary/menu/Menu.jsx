import React from 'react';
import TextareaAutosize from 'react-autosize-textarea'

const Menu = ({number, details, handleChange}) => (
   <div className="menu has-text-centered mt-40">
     <div className="title is-5">Your menu for day #{number}</div>
     <TextareaAutosize
       className="menu-description mt-20"
       value={details.breakfast}
       onChange={(e) => handleChange(e.target.value, 'breakfast', number)}
       rows={3}
       placeholder='Type here the menu for breakfast'
     />
     <TextareaAutosize
       className="menu-description mt-10"
       value={details.secondbreakfast}
       onChange={(e) => handleChange(e.target.value, 'secondbreakfast', number)}
       rows={3}
       placeholder='Type here the menu for second breakfast'
     />
     <TextareaAutosize
       className="menu-description mt-10"
       value={details.dinner}
       onChange={(e) => handleChange(e.target.value, 'dinner', number)}
       rows={3}
       placeholder='Type here the menu for dinner'
     />
     <TextareaAutosize
       className="menu-description mt-10"
       value={details.supper}
       onChange={(e) => handleChange(e.target.value, 'supper', number)}
       rows={3}
       placeholder='Type here the menu for supper'
     />
   </div>
);

export default Menu;
