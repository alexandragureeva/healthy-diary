import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'


const DiaryPage = React.createClass({

  getInitialState() {
    return {
      todayPlan: null
    }
  },

  renderNoPlanForToday() {
    console.log('render no plan for today');
    return (
      <div>
          <div className="button general right">Create Plan</div>
          <div className="arrow-container">
            <img src="/images/diary/arrow.png" className="arrow-to-plan"/>
          </div>
          <div className="in-the-middle has-text-centered">
              <h1 className="no-plan-title title is-5 pl-60 pr-60 pt-40">It looks like you eat nothing today. Lets troubleshoot it now!</h1>
          </div>
      </div>
    );
  },

  render() {
    return (
      <div className="diary-container">
        <div className="diary-box">
           {this.state.todayPlan === null && this.renderNoPlanForToday()}
        </div>
      </div>


    );
  },

})



export default DiaryPage;
