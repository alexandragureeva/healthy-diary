import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'





const Header = React.createClass({
  handleLogout(e) {
    e.preventDefault()
    if(this.props.onLogout) this.props.onLogout();
  },

  renderLoggedIn() {
    return [
      <span key={1} className="nav-item uppercase">
        <Link to="/plans">Your Plans</Link>
      </span>,

      <span key={2} className="nav-item uppercase">
        <Link to="/contact">Contact</Link>
      </span>,

      <span key={3} className="nav-item uppercase logout-button is-tab">
        <a id="logout-text" href="#" onClick={this.handleLogout}>Logout</a>
      </span>,
    ]
  },

  renderLoggedOut() {
    return (
      <span className="nav-item uppercase">
        <Link to="/login">Login</Link>
      </span>
    )
  },

  render() {
    return (
      <nav className="nav">
        <div className="container">
          {/*<!-- Left side -->*/}
          <div className="nav-left">
            <Link id="logo" className="nav-item" to="/">
              <img src="/images/logo.png" />
            </Link>
          </div>

          {/*<!-- Hamburger menu (on mobile) -->*/}
          <span id="nav-toggle" className="nav-toggle">
            <span></span>
            <span></span>
            <span></span>
          </span>

          {/*<!-- Right side -->*/}
          <div id="nav-menu" className="nav-right nav-menu">

            {this.props.user ? this.renderLoggedIn() : this.renderLoggedOut()}

          </div>
        </div>
      </nav>
    );
  },
})


export default Header
