// Dependencies for entry point
import "babel-polyfill";
import React from 'react';
import ReactDOM from 'react-dom';
import routes from './routes.jsx';
import authService from './services/auth.js'
import { Router, browserHistory, hashHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore.jsx'

//Styles used in pages
import '../../styles/importer.scss'


// Prepare store
const initialState = {
  user: authService.deserializeUser(),
};
const store = configureStore(initialState);

// Enhanced history. https://github.com/reactjs/react-router-redux
const history = syncHistoryWithStore(browserHistory, store)


if(document.getElementById('react-root')) {
  ReactDOM.render((
    <Provider store={store}>
      { /* Tell the Router to use our enhanced history */ }
      <Router history={history} children={routes} />
    </Provider>
  ), document.getElementById('react-root'));
}
