import axios from 'axios';
import { browserHistory } from 'react-router';

export default {

  getPlanInfo(planId, callback) {
    const PLAN_URL = `/api/athlete-dashboard/plan/stats/${planId}`;
    this.get(PLAN_URL, callback);
  },
//athlete-dashboard/plans
  getPlans(callback) {
    this.get(`/api/athlete-dashboard/athlete/stats`, callback);
  },

  getSpecificPlan(planId, callback) {
    const PLAN_URL = `/api/athlete-dashboard/daily/plan/stats/${planId}`;
    this.get(PLAN_URL, callback);
  },

  // getWeeklyPlanStats(planId, callback) {
  //   const PLAN_URL = `/api/athlete-dashboard/plan/${planId}`;
  //   this.get(PLAN_URL, callback);
  // },
  //
  // getDailyPlanStats(planId, callback) {
  //   const PLAN_URL = `/api/athlete-dashboard/daily/plan/stats/${planId}`;
  //   this.get(PLAN_URL, callback);
  // },

  get(url, callback) {
    const access_token  = localStorage.getItem('access_token');
    const user          = JSON.parse(localStorage.getItem('user'));

    if (access_token) {
      axios.get(url, {
        headers: {
          Authorization: `Bearer ${access_token}`,
          user: user.email,
        },
      })
        .then((response) => {
          if (response.data.error) {
            callback(500, response.data.error);
          } else {
            callback(null, response)
          }
        })
        .catch((err) => {
          console.log(err);
          if (err.response.status === 401) {
            browserHistory.push('/login');
          } else if (err.response.status == 404) {
            browserHistory.push('/notfound');
          }

          callback(new Error('Failed to get plans'))
        });
      }
    },

}
