import axios from 'axios'

const AUTH_URL = `/auth/login`;

// Authenticate on behalf of a user to receive an access token
export default {

  login: (user, callback) => {
    return axios.post(AUTH_URL, user)
      .then((response) => {
        const user = response.data;

        localStorage.setItem('access_token', user.access_token)
        localStorage.setItem('user', JSON.stringify(user))
        callback(null, user)
      })
      .catch((err) => {
        callback(new Error('Failed to login'))
      });
  },

  logout: () => {
    localStorage.removeItem('access_token')
    localStorage.removeItem('user')
  },


  loggedIn() {
    if(localStorage.getItem('access_token')) return true;
    return false;
  },

  // Retrieves user from localStorage to persist between page loads
  deserializeUser() {
    let user = null;
    try {
      user = JSON.parse(localStorage.getItem('user'))
    } catch(e) {}
    return user;
  },

}
