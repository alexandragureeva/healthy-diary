import React from 'react';
import { Route, Redirect } from 'react-router';
import authService from './services/auth'
import Layout from './components/Layout.jsx';
import LoginPage from './components/login/LoginPage.jsx';
import DiaryPage from './components/diary/DiaryPage.jsx';
import NewDiaryPage from './components/diary/NewDiaryPage.jsx';
import NotFound from './components/NotFound.jsx';
import ServerError from './components/ServerError.jsx';

const requireAuth = (nextState, replace) => {
  if( !authService.loggedIn()) {
    console.log(`${nextState.location.pathname} requires authentication`)
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }, // #todo
    })
  }
}

const routes = (
  <Route component={Layout}>
    {/*Auth*/}
    <Route path="/login" component={LoginPage} />
    <Redirect from="/" to="/diary" />
    <Route path="/diary" component={DiaryPage}/>
    <Route path="/newdiary" component={NewDiaryPage}/>



    <Route path="/whoops" component={ServerError} />
    <Route path="*" component={NotFound} />
  </Route>
);

export default routes;
//  {/*Default route*/}
//  <Redirect from="/" to="/plans" />
// <Route path="/plans" component={PlansList} onEnter={requireAuth} />
//   <Route path="/plans/:id" component={PlanStats} onEnter={requireAuth} />
//   <Route path="/contact" component={ContactPage} onEnter={requireAuth} />
