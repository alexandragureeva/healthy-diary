const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const _ = require('underscore');
const config = require('../../config');
const request = require('request');

const EncryptionService = require('../services/encryption');


/**
 * Password strategy
 */


 module.exports = new LocalStrategy({
   usernameField: 'username',
   passwordField: 'password',
   session: false,
   passReqToCallback: true
 }, (req, username, password, done) => {
   const formData = {
     username,
     password,
     grant_type: 'password',
     client_id: config.IGOR_API_CLIENT_ID,
     client_secret: config.IGOR_API_CLIENT_SECRET,
   }

   console.log(formData);

   request.post(`${config.IGOR_API_URL}/oauth/token`, {form:formData}, (err, httpResponse, body) => {
     let access_token, json;
     try {
       json = JSON.parse(body);
       access_token = json.access_token;
     } catch(e) {}
     if( !access_token) return done(new Error("Failed to login"));
     console.log('access_token before', access_token);
     // Encrypt the access token
     const encryptedToken = EncryptionService.encrypt(access_token, username);

     // This will only have data such as
     // access_token, expires_in, etc.
     const user = {
       email: username,
       access_token: encryptedToken,
       expires_in: json.expires_in,
     }
     console.log(user);

     return done(null, user);
 });
 });
