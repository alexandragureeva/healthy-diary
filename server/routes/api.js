const express = require('express');
const passport = require('passport');
const axios = require('axios');
const config = require('../../config');
const EncryptionService = require('../services/encryption');

const router = new express.Router();


router.get('/*', (req, res) => {
  console.log("In API router");

    const API_PATH        =  req.url.replace('/api/', '');
    const API_URL         = `${config.IGOR_API_URL}/${API_PATH}`
    const API_HEADERS     = req.headers;
    const encryptedToken  = API_HEADERS.authorization.substring(API_HEADERS.authorization.indexOf(' ') + 1);

    // Decrypt access token
    const decryptedToken = EncryptionService.decrypt(encryptedToken, API_HEADERS.user);

    // return axios.get(API_URL, {
    //   headers: { Authorization: `Bearer ${decryptedToken}` },
    // })
    //   .then((response) => {
    //     res.status(200).json(response.data);
    //   })
    //   .catch((err) => {
    //     console.log('API Error:', err);
    //     if (err.response) {
    //       res.status(err.response.status).json(new Error(`Failed to make API call.`));
    //     } else {
    //       res.status(500).json(new Error(`Did not get a response from the API.`));
    //     }
    //   });
    });



module.exports = router;
