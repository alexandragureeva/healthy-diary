var webpack = require('webpack');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
const passport = require('passport');
const bodyParser = require('body-parser');
var path = require('path');

var config = require('../webpack.config');

var express = new require('express');
var port = 3000;

const app = express();
var compiler = webpack(config);
app.use(webpackDevMiddleware(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));
app.use(webpackHotMiddleware(compiler));


// tell the app to look for static files in these directories
app.use(express.static('./client/dist/'));
app.use(express.static('./server/static/'));


// tell the app to parse HTTP body messages
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// pass the passport middleware
app.use(passport.initialize());
app.use(passport.session());

// routes
// const authRoutes = require('./routes/auth');
// const apiRoutes = require('./routes/api');
// app.use('/auth', authRoutes);
// app.use('/api', apiRoutes);

// load passport strategies
// const localStrategy = require('./passport/local');
// passport.use('local', localStrategy);

// pass the authenticaion checker middleware
// const authCheckMiddleware = require('./middleware/isAuthenticated');
// app.use('/api', authCheckMiddleware);

app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname+'/static/index.html'));
});


app.listen(port, function(error) {
  if (error) {
    console.error(error);
  } else {
    console.info('==> ??  Open up http://localhost:%s/ in your browser.', port);
  }
});
